import heapq
MIN = -10000
MAX = 10000
faza = None


def createBoard():
    board = {}
    for i in range(24):
        #   rb   h   v
        b = [i, [], []]
        board[i] = (b)
    board[0][1].extend([1])
    board[0][2].extend([9])
    board[1][1].extend([0, 2])
    board[1][2].extend([4])
    board[2][1].extend([1])
    board[2][2].extend([14])
    board[3][1].extend([4])
    board[3][2].extend([10])
    board[4][1].extend([3, 5])
    board[4][2].extend([7, 1])
    board[5][1].extend([4])
    board[5][2].extend([13])
    board[6][1].extend([7])
    board[6][2].extend([11])
    board[7][1].extend([6, 8])
    board[7][2].extend([4])
    board[8][1].extend([7])
    board[8][2].extend([12])
    board[9][1].extend([10])
    board[9][2].extend([0, 21])
    board[10][1].extend([11, 9])
    board[10][2].extend([3, 18])
    board[11][1].extend([10])
    board[11][2].extend([6, 15])
    board[12][1].extend([13])
    board[12][2].extend([8, 17])
    board[13][1].extend([12, 14])
    board[13][2].extend([5, 20])
    board[14][1].extend([13])
    board[14][2].extend([2, 23])
    board[15][1].extend([16])
    board[15][2].extend([11])
    board[16][1].extend([15, 17])
    board[16][2].extend([19])
    board[17][1].extend([16])
    board[17][2].extend([12])
    board[18][1].extend([19])
    board[18][2].extend([10])
    board[19][1].extend([18, 20])
    board[19][2].extend([16, 22])
    board[20][1].extend([19])
    board[20][2].extend([13])
    board[21][1].extend([22])
    board[21][2].extend([9])
    board[22][1].extend([21, 23])
    board[22][2].extend([19])
    board[23][1].extend([22])
    board[23][2].extend([14])
    return board


board = createBoard()
izlaz = {}


class TreeNode:
    def __init__(self, data=None):
        self.data = data
        self.value = None
        self.children = []
        self.parent = None
        self.player = None
        self.num_of_morrises = None
        self.closed_morris = None
        self.morris_coords = []
        self.last_played = None
    def __lt__(self, other):
        if self.value == None:
            self.value = calculate_value(self)
        if other.value == None:
            other.value = calculate_value(other)
        return self.value < other.value

class Tree:
    def __init__(self, root):
        self.root = root

cnik = {
    "W": [MIN, max, "max", 1],
    "B": [MAX, min, "min", -1]
}
def maxkeycvorcreate():
    #######    h   v
    keycvor = []
    for index, cvor in board.items():
        if len(cvor[1]) == 2 and len(cvor[2]) == 2:
            keycvor.append(index)
    return keycvor


maxkeycvor = maxkeycvorcreate()
def possible(stablo, move):
    best = cnik[player][0]
    moguci = []
    per_con = [[], [], [], []]
    for child in stablo.root.children:
        if move == "INIT" or move == "MILL":
            per_con[len(board[child.last_played][1] + board[child.last_played][2])-1].append(child)
        else:
            per_con[len(board[child.last_played[1]][1] + board[child.last_played[1]][2])-1].append(child)
    what = []
    for c in per_con:
        what.extend(c)
    stablo.root.children = what[::-1]
    for child in stablo.root.children:
        printaboard(child.data)
        print(child.value, calculate_value(child))
        if child.value == None:
            continue
        former_best = best
        if move == "MILL":
            best = cnik[child.player][1](best, child.value)
            if best != former_best:
                moguci = []
                moguci.append(child)
        else:
            if player == "W":
                if child.value > best:
                    moguci = []
                    moguci.append(child)
                    best = child.value
                elif child.value == best:
                    moguci.append(child)

            elif player == "B":
                if child.value < best:
                    moguci = []
                    moguci.append(child)
                    best = child.value
                elif child.value == best:
                    moguci.append(child)
    return moguci

def nextMove(player, move, boardString):
    global faza, board
    depth_init = 4
    depth_move = 4
    board = createBoard()
    root = TreeNode(getData(boardString))
    if move == "MILL":
        root.player = player
        root = make_parent(root, player)
        stablo = Tree(root)
    else:
        root.player = "WB".replace(player, "")
        faza = move
        stablo = Tree(root)
        root.children = make_children(root)
        if root.player == "W":
            heapq.heapify(root.children)
        else:
            heapq.heapify(root.children)
            root.children = root.children[::-1]

    for child in root.children:
        if faza == "INIT":
            if root.data.count(player) < 3:
                alphabeta(child, MIN, MAX, 1)
            else:
                alphabeta(child, MIN, MAX, 1)
        elif faza == "MOVE":
            if root.data.count("W") < 4 or root.data.count("B") < 4:
                alphabeta(child, MIN, MAX, 1)
            else:
                alphabeta(child, MIN, MAX, 1)

    moguci = possible(stablo, faza)
    trazeni = moguci[0]

    if faza == "INIT" or move == "MILL":
        print(izlaz[trazeni.last_played][0], izlaz[trazeni.last_played][1])
    elif faza == "MOVE":
        print(izlaz[trazeni.last_played[0]][0], izlaz[trazeni.last_played[0]][1], end=" ")
        print(izlaz[trazeni.last_played[1]][0], izlaz[trazeni.last_played[1]][1])




def alphabeta(node, alpha, beta, depth):
    if depth == 0:
        if node.value == None:
            node.value = calculate_value(node)
        return node.value

    if node.player == "W":
        best = MAX
        if len(node.children) == 0:
            node.children = make_children(node)
            heapq.heapify(node.children)
        for child in node.children:
            value = alphabeta(child, alpha, beta, depth - 1)
            best = min(best, value)
            beta = min(beta, best)
            if beta <= alpha:
                break
        node.value = best
        return best
    else:
        best = MIN
        if len(node.children) == 0:
            node.children = make_children(node)
            heapq.heapify(node.children)
        for child in node.children[::-1]:
            value = alphabeta(child, alpha, beta, depth - 1)
            best = max(best, value)
            alpha = max(alpha, best)
            if beta <= alpha:
                break
        node.value = best
        return best


def make_children(node):
    children = []
    opponent = "WB".replace(node.player, "")
    if node.value != None:
        if node.value > 1000 or node.value < -1000:
            newchild = TreeNode([*node.data])
            newchild.parent = node
            newchild.player = node.player
            newchild.closed_morris = newchild.closed_morris
            newchild.num_of_morrises = newchild.num_of_morrises
            newchild.morris_coords = newchild.morris_coords
            newchild.value = node.value
            children.append(newchild)
            return children
    for move in get_possible_moves(node):
        newchild = TreeNode([*node.data])
        newchild.parent = node
        if faza == "INIT":
            newchild.data[move[0]] = move[1]
            newchild.last_played = move[0]
            newchild.player = move[1]
            if is_mill(newchild) == cnik[move[1]][3]:
                for millmove in get_mill_moves(newchild):
                    newchild2 = TreeNode([*newchild.data])
                    newchild2.data[millmove[0]] = millmove[1]
                    newchild2.parent = node
                    newchild2.closed_morris = newchild.closed_morris
                    newchild2.num_of_morrises = newchild.num_of_morrises
                    newchild2.morris_coords = newchild.morris_coords
                    newchild2.player = move[1]
                    newchild2.last_played = newchild.last_played
                    children.append(newchild2)
            else:
                children.append(newchild)
        elif faza == "MOVE":
            if len(move) == 4:
                            ## i        opponent
                newchild.data[move[0]] = "O"
                newchild.data[move[1]] = move[2]
                newchild.player = move[2]
                newchild.last_played = (move[0], move[1])
                if is_mill(newchild) == cnik[move[2]][3]:
                    for millmove in get_mill_moves(newchild):
                        newchild2 = TreeNode([*newchild.data])
                        newchild2.data[millmove[0]] = millmove[1]
                        newchild2.parent = node
                        newchild2.closed_morris = newchild.closed_morris
                        newchild2.num_of_morrises = newchild.num_of_morrises
                        newchild2.morris_coords = newchild.morris_coords
                        newchild2.player = move[2]
                        newchild2.last_played = (move[0], move[1])
                        children.append(newchild2)
                else:
                    children.append(newchild)
            else:
                newchild.data[move[0]] = "O"
                newchild.data[move[1]] = move[2]
                newchild.player = move[2]
                newchild.last_played = (move[0], move[1])
                if is_mill(newchild) == cnik[move[2]][3]:
                    for millmove in get_mill_moves(newchild):
                        newchild2 = TreeNode([*newchild.data])
                        newchild2.data[millmove[0]] = millmove[1]
                        newchild2.parent = node
                        newchild2.closed_morris = newchild.closed_morris
                        newchild2.num_of_morrises = newchild.num_of_morrises
                        newchild2.morris_coords = newchild.morris_coords
                        newchild2.player = move[2]
                        newchild2.last_played = (move[0], move[1])
                        children.append(newchild2)
                else:
                    children.append(newchild)
    if len(children) == 0:
        children.append(node)
    return children


def get_possible_moves(node):
    moves = []
    opponent = "WB".replace(node.player, "")
    if faza == "INIT":
        for i, d in enumerate(node.data):
            if d == "O":
                moves.append((i, opponent, False))
    else:
        broj = node.data.count(opponent)
        if broj == 3:
            for i, d in enumerate(node.data):
                if d == opponent:
                    for ij, j in enumerate(node.data):
                        if j == "O":
                            moves.append((i, ij, opponent, False))
        else:
            for i, d in enumerate(node.data):
                if d == opponent:
                    for j in board[i][1] + board[i][2]:
                        if node.data[j] == "O":
                            moves.append((i, j, opponent))
    return moves


def get_mill_moves(node):
    opponent = "WB".replace(node.player, "")
    moves = []
    for i, d in enumerate(node.data):
        if d == opponent:
            moves.append((i, "O"))
    return moves


def make_parent(node, player):
    global faza
    faza = "INIT"
    parent = TreeNode(node.data)
    parent.player = "WB".replace(player, "")
    for move in get_mill_moves(node):
        newchild2 = TreeNode([*node.data])
        newchild2.data[move[0]] = move[1]
        newchild2.parent = node
        newchild2.player = player
        newchild2.last_played = move[0]
        node.children.append(newchild2)
    return node


###
def keycvorcreate():
    #######    h   v
    keycvor = [[], []]
    for index, cvor in board.items():
        if len(cvor[1]) == 2:
            keycvor[0].append(index)
        if len(cvor[2]) == 2:
            keycvor[1].append(index)
    return keycvor


keycvor = keycvorcreate()


def closed_morris1(node, player=None):
    global board
    roditelj = node.parent
    ja = node
    if roditelj == None:
        return 0
    if roditelj.num_of_morrises == None:
        roditelj.num_of_morrises = num_of_morrises2(node.parent)
    ja.num_of_morrises = num_of_morrises2(node)

    if roditelj.num_of_morrises < ja.num_of_morrises:
        ja.closed_morris = 1
        return 1
    if roditelj.num_of_morrises > ja.num_of_morrises:
        ja.closed_morris = -1
        return -1
    ja.closed_morris = 0
    return 0


def num_of_morrises2(node):
    white = 0
    black = 0
    data = node.data
    for index in keycvor[0]:
        morris = True
        if data[index] == "W":
            for i in board[index][1]:
                if data[i] != "W":
                    morris = False
            if morris:
                white += 1
                node.morris_coords.extend(board[index][1])
        elif data[index] == "B":
            for i in board[index][1]:
                if data[i] != "B":
                    morris = False
            if morris:
                black += 1
                node.morris_coords.extend(board[index][1])
    for index in keycvor[1]:
        morris = True
        if data[index] == "W":
            for i in board[index][2]:
                if data[i] != "W":
                    morris = False
            if morris:
                white += 1
                node.morris_coords.extend(board[index][2])
        elif data[index] == "B":
            for i in board[index][2]:
                if data[i] != "B":
                    morris = False
            if morris:
                black += 1
                node.morris_coords.extend(board[index][2])
    return white - black


def phase1(node):
    # blocked, num of pieces, three piece, two piece
    global board
    data = node.data
    blocked_score = 0
    num_of_pieces_score = 0
    kords_double = []
    two_piece_score = 0
    three_piece_score = 0
    for index, cvor in board.items():
        blocked = True
        if data[index] == "W":
            num_of_pieces_score += 1
            if len(cvor[1]) == 2:
                whiteincvor = 0
                oincvor = 0
                for i in cvor[1]:
                    if data[i] == "W":
                        whiteincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if whiteincvor == oincvor == 1:
                    kords_double.extend(cvor[1])
                    two_piece_score += 1
            if len(cvor[2]) == 2:
                whiteincvor = 0
                oincvor = 0
                for i in cvor[2]:
                    if data[i] == "W":
                        whiteincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if whiteincvor == oincvor == 1:
                    kords_double.extend(cvor[2])
                    two_piece_score += 1

            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False

            if blocked:
                blocked_score -= 1

        elif data[index] == "B":
            num_of_pieces_score -= 1
            if len(cvor[1]) == 2:
                blackincvor = 0
                oincvor = 0
                for i in cvor[1]:
                    if data[i] == "B":
                        blackincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if blackincvor == oincvor == 1:
                    kords_double.extend(cvor[1])
                    two_piece_score -= 1

            if len(cvor[2]) == 2:
                blackincvor = 0
                oincvor = 0
                for i in cvor[2]:
                    if i == "B":
                        blackincvor += 1
                    elif i == "O":
                        oincvor += 1
                if blackincvor == oincvor == 1:
                    kords_double.extend(cvor[2])
                    two_piece_score -= 1

            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False

            if blocked:
                blocked_score += 1

    been = []
    white_three = black_three = 0
    for k in kords_double:
        if k not in been:
            been.append(k)
        else:
            if data[k] == "W":
                white_three += 1
            else:
                black_three += 1
    three_piece_score = white_three - black_three
    if node.closed_morris == None or node.num_of_morrises == None:
        node.closed_morris = 0
        node.num_of_morrises = 0
    return 18 * node.closed_morris + 26 * node.num_of_morrises + blocked_score + 10 * two_piece_score \
           + num_of_pieces_score + 7 * three_piece_score


def phase2(node, num_of_whites, num_of_blacks):
    data = node.data
    white_blocked = 0
    black_blocked = 0
    winning_score = 0
    double_morris_score = 0
    for index, cvor in board.items():
        if data[index] == "W":
            blocked = True
            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False
            if blocked:
                white_blocked += 1

        elif data[index] == "B":
            blocked = True
            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False
            if blocked:
                black_blocked += 1
    been = []
    white = black = 0
    for k in node.morris_coords:
        if k not in been:
            been.append(k)
        else:
            if node.data[k] == "W":
                white += 1
            else:
                black += 1
    double_morris_score = white - black
    if black_blocked == num_of_blacks or num_of_blacks < 3:
        winning_score = 1
    if white_blocked == num_of_whites or num_of_whites < 3:
        winning_score = -1
    blocked_score = black_blocked - white_blocked
    if node.closed_morris == None or node.num_of_morrises == None:
        node.closed_morris = 0
        node.num_of_morrises = 0
    return 50 * node.closed_morris + 43 * node.num_of_morrises + 4 * blocked_score + 8 * double_morris_score + 1200 * winning_score


def phase3(node, num_of_whites, num_of_blacks):
    data = node.data
    two_piece_score = 0
    three_piece_score = 0
    white_blocked = 0
    black_blocked = 0
    koords_double = []
    for index, cvor in board.items():
        blocked = True
        blocked = True
        if data[index] == "W":
            if len(cvor[1]) == 2:
                whiteincvor = 0
                oincvor = 0
                for i in cvor[1]:
                    if data[i] == "W":
                        whiteincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if whiteincvor == oincvor == 1:
                    koords_double.extend(cvor[1])
                    two_piece_score += 1
            if len(cvor[2]) == 2:
                whiteincvor = 0
                oincvor = 0
                for i in cvor[2]:
                    if data[i] == "W":
                        whiteincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if whiteincvor == oincvor == 1:
                    koords_double.extend(cvor[2])
                    two_piece_score += 1

            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False

            if blocked:
                white_blocked += 1

        elif data[index] == "B":
            if len(cvor[1]) == 2:
                blackincvor = 0
                oincvor = 0
                for i in cvor[1]:
                    if data[i] == "B":
                        blackincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if blackincvor == oincvor == 1:
                    koords_double.extend(cvor[1])
                    two_piece_score -= 1

            if len(cvor[2]) == 2:
                blackincvor = 0
                oincvor = 0
                for i in cvor[2]:
                    if data[i] == "B":
                        blackincvor += 1
                    elif data[i] == "O":
                        oincvor += 1
                if blackincvor == oincvor == 1:
                    koords_double.extend(cvor[2])
                    two_piece_score -= 1

            for item in cvor[1] + cvor[2]:
                if data[item] == "O":
                    blocked = False

            if blocked:
                black_blocked += 1

    been = []
    white_three = black_three = 0
    for k in koords_double:
        if k not in been:
            been.append(k)
        else:
            if node.data[k] == "W":
                white_three += 1
            else:
                black_three += 1
    winning_score = 0
    if black_blocked == num_of_blacks or num_of_blacks < 3:
        winning_score = 1
    if white_blocked == num_of_whites or num_of_whites < 3:
        winning_score = -1
    three_piece_score = white_three - black_three
    if node.closed_morris == None or node.num_of_morrises == None:
        node.closed_morris = 0
        node.num_of_morrises = 0
    return 16 * node.closed_morris + 43 * node.num_of_morrises + 10 * two_piece_score + 1*three_piece_score + 1200 * winning_score

def calculate_value(node):
    global board
    data = node.data
    blacks = 0
    whites = 0
    if faza == "INIT":
        sum = phase1(node)
    else:
        for i in data:
            if i == "W":
                whites += 1
            elif i == "B":
                blacks += 1
        if faza == "MOVE" and whites > 3 and blacks > 3:
            sum = phase2(node, whites, blacks) + 40 * (whites - blacks)
        elif faza == "MOVE":
            sum = phase3(node, whites, blacks) + 40 * (whites - blacks)

    return sum


def is_mill(x):
    if x == None or x.parent == None:
        return 0
    return closed_morris1(x, x.player)


def getData(board):
    global izlaz
    data = []
    x = y = k = 0
    board = board.replace(" ", "")
    for i in board:
        if i in "WBO":
            data.append(i)
            izlaz[k] = (x, y)
            k += 1
        y += 1
        if i == "\n":
            x += 1
            y = 0
    return data


def printaboard(data):
    boardStr = """ 
                {}--{}--{}
                |{}-{}-{}| 
                ||{}{}{}|| 
                {}{}{}*{}{}{}
                ||{}{}{}|| 
                |{}-{}-{}| 
                {}--{}--{} """.format(*data)
    print(boardStr)


if __name__ == "__main__":
    player = input()
    faza = input()
    lines = []
    no_of_lines = 7
    lines = ""
    for i in range(no_of_lines):
        lines += input() + "\n"
    nextMove(player, faza, lines)

