from hackerrank import printaboard, TreeNode, board, createBoard, nextMove, is_mill

node = TreeNode()
faza = 1
broj_poteza = 0
board = createBoard()
mill = False


def player_move():
    global mill
    set_faza()
    print("Trenutno stanje ->")
    printaboard(node.data)
    get_move()
    printaboard(node.data)
    if is_mill(node) == 1:
        mill = True
        player_move()
    pass


def set_faza():
    global faza
    if broj_poteza >= 18:
        if node.data.count("W") > 3:
            faza = 2
        else:
            faza = 3


def get_move():
    global mill

    if mill:
        while True:
            coord = int(input("Unesite koordinatu: ")) - 1
            if 0 <= coord <= 23 and node.data[coord] == "B":
                node.data[coord] = "O"
                node.parent = None
                mill = False
                return

    if faza == 1:
        while True:
            coord = int(input("Unesite koordinatu: ")) - 1
            if 0 <= coord <= 23 and node.data[coord] == "O":
                newnode = TreeNode()
                newnode = TreeNode([*node.data])
                node.data[coord] = "W"
                node.parent = newnode
                break

    elif faza == 2:
        while True:
            coord_from = int(input("Unesite koordinatu sa koje se pomerate: ")) - 1
            coord_to = int(input("Unesite koordinatu na koju se pomerate: ")) - 1
            if 0 <= coord_from <= 23 and node.data[coord_from] == "W":
                if 0 <= coord_to <= 23 and node.data[coord_to] == "O":
                    print(board[coord_from][1] + board[coord_to][2])
                    if coord_to in board[coord_from][1] + board[coord_from][2]:
                        newnode = TreeNode()
                        newnode = TreeNode([*node.data])
                        node.parent = newnode
                        node.data[coord_from] = "O"
                        node.data[coord_to] = "W"
                        break
    elif faza == 3:
        while True:
            coord_from = int(input("Unesite koordinatu sa koje se pomerate: ")) - 1
            coord_to = int(input("Unesite koordinatu na koju se pomerate: ")) - 1
            if 0 <= coord_from <= 23 and node.data[coord_from] == "W":
                if 0 <= coord_to <= 23 and node.data[coord_to] == "O":
                    newnode = TreeNode()
                    newnode = TreeNode([*node.data])
                    node.parent = newnode
                    node.data[coord_from] = "O"
                    node.data[coord_to] = "W"
                    break


def bot_move():
    global node
    print("Odigran potez ->")
    if faza == 1:
        node = nextMove("B", "INIT", printaboard(node.data, False))
    elif faza == 2 or faza == 3:
        node = nextMove("B", "MOVE", printaboard(node.data, False))
    printaboard(node.data)


def make_board():
    board_data = ["O"] * 24
    node.data = board_data


if __name__ == "__main__":
    make_board()
    while True:
        player_move()
        bot_move()
        broj_poteza += 2
