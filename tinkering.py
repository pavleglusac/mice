from timeit import default_timer as timer
MIN = -10000
MAX = 10000
faza = None
board = None


def createBoard():
    global board
    board = {}
    for i in range(24):
        #   rb   h   v
        b = [i, [], []]
        board[i] = (b)
    board[0][1].extend([1])
    board[0][2].extend([9])
    board[1][1].extend([0, 2])
    board[1][2].extend([4])
    board[2][1].extend([1])
    board[2][2].extend([14])
    board[3][1].extend([4])
    board[3][2].extend([10])
    board[4][1].extend([3, 5])
    board[4][2].extend([7, 1])
    board[5][1].extend([4])
    board[5][2].extend([13])
    board[6][1].extend([7])
    board[6][2].extend([11])
    board[7][1].extend([6, 8])
    board[7][2].extend([4])
    board[8][1].extend([7])
    board[8][2].extend([12])
    board[9][1].extend([10])
    board[9][2].extend([0, 21])
    board[10][1].extend([11, 9])
    board[10][2].extend([3, 18])
    board[11][1].extend([10])
    board[11][2].extend([6, 15])
    board[12][1].extend([13])
    board[12][2].extend([8, 17])
    board[13][1].extend([12, 14])
    board[13][2].extend([5, 20])
    board[14][1].extend([13])
    board[14][2].extend([2, 23])
    board[15][1].extend([16])
    board[15][2].extend([11])
    board[16][1].extend([15, 17])
    board[16][2].extend([19])
    board[17][1].extend([16])
    board[17][2].extend([12])
    board[18][1].extend([19])
    board[18][2].extend([10])
    board[19][1].extend([18, 20])
    board[19][2].extend([16, 22])
    board[20][1].extend([19])
    board[20][2].extend([13])
    board[21][1].extend([22])
    board[21][2].extend([9])
    board[22][1].extend([21, 23])
    board[22][2].extend([19])
    board[23][1].extend([22])
    board[23][2].extend([14])
    return board


izlaz = {}


class TreeNode:
    def __init__(self, data=None):
        self.data = data
        self.value = None
        self.children = []
        self.parent = None
        self.player = None
        self.num_of_morrises = None
        self.last_played = None


class Tree:
    def __init__(self, root):
        self.root = root


cnik = {
    "W": [MIN, max, "max", 1],
    "B": [MAX, min, "min", -1]
}


def nextMove(player, move, boardString):
    global faza, board
    board = createBoard()
    root = TreeNode(getData(boardString))
    if move == "MILL":
        root.player = player
        root = make_parent(root, player)
        stablo = Tree(root)
    else:
        root.player = "WB".replace(player, "")
        faza = move
        stablo = Tree(root)
    if faza == "INIT":
        alphabeta(stablo.root, MIN, MAX, 4)
    elif faza == "MOVE":
        alphabeta(stablo.root, MIN, MAX, 4)
    best = cnik[player][0]
    moguci = []
    for child in stablo.root.children:
        # printaboard(child.data)
        # print(child.value, calculate_value(child), child.player)
        if child.value == None:
            continue
        former_best = best
        if move == "MILL":
            best = cnik[child.player][1](best, child.value)
            if best != former_best:
                moguci = []
                moguci.append(child)
        else:
            if player == "W":
                if child.value > best:
                    moguci = []
                    moguci.append(child)
                    best = child.value
                elif child.value == best:
                    moguci.append(child)

            elif player == "B":
                if child.value < best:
                    moguci = []
                    moguci.append(child)
                    best = child.value
                elif child.value == best:
                    moguci.append(child)

    if len(moguci) > 1:
        best = cnik[player][0]
        for moguc in moguci.copy():
            if moguc.player == "W":
                if best < calculate_value(moguc):
                    moguci[0] = moguc
                    best = calculate_value(moguc)
            elif moguc.player == "B":
                if best > calculate_value(moguc):
                    moguci[0] = moguc
                    best = calculate_value(moguc)

    trazeni = moguci[0]

    if faza == "INIT" or move == "MILL":
        print(izlaz[trazeni.last_played][0], izlaz[trazeni.last_played][1])
    elif faza == "MOVE":
        print(izlaz[trazeni.last_played[0]][0], izlaz[trazeni.last_played[0]][1], end=" ")
        print(izlaz[trazeni.last_played[1]][0], izlaz[trazeni.last_played[1]][1])
    printaboard(trazeni.data)
    print(trazeni.value, calculate_value(trazeni), trazeni.player)


def alphabeta(node, alpha, beta, depth):
    if depth == 0:
        node.value = calculate_value(node)
        return node.value
    if faza == "INIT":
        trazeni = "W"
    elif faza == "MOVE":
        trazeni = "W"
    if node.player == trazeni:
        best = MAX
        if len(node.children) == 0:
            node.children = make_children(node)
        for child in node.children:
            value = alphabeta(child, alpha, beta, depth - 1)
            best = min(best, value)
            beta = min(beta, best)
            if beta <= alpha:
                break
        node.value = best
        return best
    else:
        best = MIN
        if len(node.children) == 0:
            node.children = make_children(node)
        for child in node.children:
            value = alphabeta(child, alpha, beta, depth - 1)
            best = max(best, value)
            alpha = max(alpha, best)
            if beta <= alpha:
                break
        node.value = best
        return best


def make_children(node):
    children = []
    opponent = "WB".replace(node.player, "")
    for move in get_possible_moves(node):
        newchild = TreeNode([*node.data])
        newchild.parent = node
        if faza == "INIT":
            newchild.data[move[0]] = move[1]
            newchild.last_played = move[0]
            newchild.player = move[1]
            if is_mill(newchild) == cnik[move[1]][3]:
                for millmove in get_mill_moves(newchild):
                    newchild2 = TreeNode([*newchild.data])
                    newchild2.data[millmove[0]] = millmove[1]
                    newchild2.parent = node
                    newchild2.player = move[1]
                    newchild2.last_played = newchild.last_played
                    children.append(newchild2)
            else:
                children.append(newchild)
        elif faza == "MOVE":
            if move[2] == False:
                newchild.data[move[0]] = move[1]
                newchild.player = move[1]
                newchild.last_played = move[0]
                if is_mill(newchild) == cnik[move[1]][3]:
                    for millmove in get_mill_moves(newchild):
                        newchild2 = TreeNode([*newchild.data])
                        newchild2.data[millmove[0]] = millmove[1]
                        newchild2.parent = node
                        newchild2.player = move[1]
                        newchild2.last_played = move[0]
                        children.append(newchild2)
                else:
                    children.append(newchild)
            else:
                newchild.data[move[0]] = "O"
                newchild.data[move[1]] = move[2]
                newchild.player = move[2]
                newchild.last_played = (move[0], move[1])
                if is_mill(newchild) == cnik[move[2]][3]:
                    for millmove in get_mill_moves(newchild):
                        newchild2 = TreeNode([*newchild.data])
                        newchild2.data[millmove[0]] = millmove[1]
                        newchild2.parent = node
                        newchild2.player = move[2]
                        newchild2.last_played = (move[0], move[1])
                        children.append(newchild2)
                else:
                    children.append(newchild)
    if len(children) == 0:
        children.append(node)
    return children


def get_possible_moves(node):
    moves = []
    opponent = "WB".replace(node.player, "")
    if faza == "INIT":
        for i, d in enumerate(node.data):
            if d == "O":
                moves.append((i, opponent, False))
    else:
        if node.data.count(opponent) == 3:
            for i, d in enumerate(node.data):
                if d == "O":
                    moves.append((i, opponent, False))
        else:
            for i, d in enumerate(node.data):
                if d == opponent:
                    for j in board[i][1] + board[i][2]:
                        if node.data[j] == "O":
                            moves.append((i, j, opponent))
    return moves


def get_mill_moves(node):
    opponent = "WB".replace(node.player, "")
    moves = []
    for i, d in enumerate(node.data):
        if d == opponent:
            moves.append((i, "O"))
    return moves


def make_parent(node, player):
    global faza
    faza = "MOVE"
    parent = TreeNode(node.data)
    for move in get_mill_moves(node):
        newchild2 = TreeNode([*node.data])
        newchild2.data[move[0]] = move[1]
        newchild2.parent = node
        newchild2.player = player
        newchild2.last_played = move[0]
        parent.children.append(newchild2)
    parent.player = "WB".replace(player, "")
    return parent


###
kords_double = []
kords = []


def closed_morris1(node, player=None):
    global board
    if node.parent == None:
        return 0
    roditelj = num_of_morrises2(node.parent, node.player)
    ja = num_of_morrises2(node, node.player)
    if roditelj < ja:
        return 1
    if roditelj > ja:
        return -1
    return 0


def num_of_morrises2(node, player=None):
    global board, kords
    if node.num_of_morrises != None:
        return node.num_of_morrises
    kords = []
    data = node.data
    white = black = 0
    for index, cvor in board.items():
        p = True
        if data[index] == "W":
            if player == "B":
                continue
            if len(cvor[1]) == 2:
                for k in cvor[1]:
                    if data[k] != "W":
                        p = False
                if p:
                    kords.append(index)
                    kords.extend(cvor[1])
                    white += 1
            p = True
            if len(cvor[2]) == 2:
                for k in cvor[2]:
                    if data[k] != "W":
                        p = False
                if p:
                    kords.append(index)
                    kords.extend(cvor[2])
                    white += 1

        elif data[index] == "B":
            if player == "W":
                continue
            if len(cvor[1]) == 2:
                for k in cvor[1]:
                    if data[k] != "B":
                        p = False
                if p:
                    kords.append(index)
                    kords.extend(cvor[1])
                    black += 1
            p = True
            if len(cvor[2]) == 2:
                for k in cvor[2]:
                    if data[k] != "B":
                        p = False
                if p:
                    kords.append(index)
                    kords.extend(cvor[1])
                    black += 1
    return white - black


def num_of_blocked3(node):
    global board
    data = node.data
    white = black = 0
    for index, cvor in board.items():
        p = True
        if data[index] == "W":
            for k in cvor[1] + cvor[2]:
                if data[k] == "O":
                    p = False
            if p:
                white += 1
            p = True

        elif data[index] == "B":
            for k in cvor[1] + cvor[2]:
                if data[k] == "O":
                    p = False
            if p:
                black += 1
            p = True
    return black - white


def num_of_pieces4(node):
    data = node.data
    white = 0
    black = 0
    for d in data:
        if d == "W":
            white += 1
        elif d == "B":
            black += 1
    return white - black


def num_of_2piece_config5(node):
    global board, kords_double
    data = node.data
    white = black = 0
    kords_double = []
    for index, cvor in board.items():
        if data[cvor[0]] == "W":
            if len(cvor[1]) == 2:
                nule = crni = 0
                for k in cvor[1]:
                    if data[k] == "W":
                        crni += 1
                    if data[k] == "O":
                        nule += 1
                if crni == nule == 1:
                    kords_double.append(index)
                    white += 1
            if len(cvor[2]) == 2:
                nule = crni = 0
                for k in cvor[2]:
                    if data[k] == "W":
                        crni += 1
                    if data[k] == "O":
                        nule += 1
                if crni == nule == 1:
                    kords_double.append(index)
                    white += 1

        elif data[index] == "B":
            if len(cvor[1]) == 2:
                nule = crni = 0
                for k in cvor[1]:
                    if data[k] == "B":
                        crni += 1
                    if data[k] == "O":
                        nule += 1
                if crni == nule == 1:
                    kords_double.append(index)
                    black += 1
            if len(cvor[2]) == 2:
                nule = crni = 0
                for k in cvor[2]:
                    if data[k] == "B":
                        crni += 1
                    if data[k] == "O":
                        nule += 1
                if crni == nule == 1:
                    kords_double.append(index)
                    black += 1
        else:
            if len(cvor[1]) == 2:
                crni = beli = 0
                for k in cvor[1]:
                    if data[k] == "B":
                        crni += 1
                    if data[k] == "W":
                        beli += 1
                if crni == 2:
                    kords_double.extend(cvor[1])
                    black += 1
                elif beli == 2:
                    kords_double.extend(cvor[1])
                    white += 1
            if len(cvor[2]) == 2:
                crni = beli = 0
                for k in cvor[2]:
                    if data[k] == "B":
                        crni += 1
                    if data[k] == "W":
                        beli += 1
                if crni == 2:
                    kords_double.extend(cvor[2])
                    black += 1
                elif beli == 2:
                    kords_double.extend(cvor[2])
                    white += 1
    return white - black


def num_of_3piececofnig6(node):
    global board, kords_double
    data = node.data
    been = []
    white = black = 0
    for k in kords_double:
        if k not in been:
            been.append(k)
        else:
            if data[k] == "W":
                white += 1
            else:
                black += 1
    return white - black


def double_morris7(node):
    global kords
    been = []
    white = black = 0
    for k in kords:
        if k not in been:
            been.append(k)
        else:
            if node.data[k] == "W":
                white += 1
            else:
                black += 1
    return white - black


def winning_config8(node):
    whites = blacks = 0
    blocked_white = blocked_black = 0
    for index, cvor in board.items():
        p = True
        if node.data[index] == "W":
            whites += 1
            for neki in cvor[1] + cvor[2]:
                if node.data[neki] == "O":
                    p = False
            if p:
                blocked_white += 1
        elif node.data[index] == "B":
            blacks += 1
            for neki in cvor[1] + cvor[2]:
                if node.data[neki] == "O":
                    p = False
            if p:
                blocked_black += 1
    if blocked_black == blacks or blacks < 3:
        return 1
    if blocked_white == whites or whites < 3:
        return -1
    return 0


def calculate_value(node):
    global board
    data = node.data
    blacks = node.data.count("B")
    whites = node.data.count("W")
    if faza == "INIT":
        sum = 18 * closed_morris1(node) + 26 * num_of_morrises2(node) + num_of_blocked3(node) + 9 * num_of_pieces4(
            node) + 10 * num_of_2piece_config5(node) + 7 * num_of_3piececofnig6(node)
    if faza == "MOVE" and (blacks > 3 and whites > 3):
        sum = 60 * closed_morris1(node) + 23 * num_of_morrises2(node) + 10 * num_of_blocked3(
            node) + 21 * num_of_pieces4(node)
        +8 * double_morris7(node) + 1086 * winning_config8(node)
    elif faza == "MOVE":
        sum = 16 * closed_morris1(node) + 10 * num_of_2piece_config5(node) + num_of_3piececofnig6(
            node) + 1190 * winning_config8(node)
    return sum


def is_mill(x):
    if x == None or x.parent == None:
        return 0
    return closed_morris1(x, x.player)


###


def getData(board):
    global izlaz
    data = []
    x = y = k = 0
    board = board.replace(" ", "")
    for i in board:
        if i in "WBO":
            data.append(i)
            izlaz[k] = (x, y)
            k += 1
        y += 1
        if i == "\n":
            x += 1
            y = 0
    return data


def printaboard(data):
    boardStr = """ 
                {}--{}--{}
                |{}-{}-{}| 
                ||{}{}{}|| 
                {}{}{}*{}{}{}
                ||{}{}{}|| 
                |{}-{}-{}| 
                {}--{}--{} """.format(*data)
    print(boardStr)


if __name__ == "__main__":
    player = input()
    faza = input()
    lines = []
    no_of_lines = 7
    lines = ""
    for i in range(no_of_lines):
        lines += input() + "\n"
    t = timer()
    nextMove(player, faza, lines)
    print(timer() - t)
