from neomorris import phase1, TreeNode, getData, board, createBoard, phase3, phase2, get_possible_moves
board = createBoard()

s = """
B--O--O
                |O-W-O| 
                ||OOO|| 
                BOW*OOO
                ||WBO|| 
                |B-B-B| 
                B--B--B """
t = TreeNode(getData(s.strip()))
t.player = "B"
print(phase2(t, s.count("W"), s.count("B")))