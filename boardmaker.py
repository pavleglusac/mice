s = """O--O--O
        |O-O-O|
        ||OOO||
        OOO*OOO
        ||OOO||
        |O-O-O|
        O--O--O"""
s = s.replace(" ", "")
tabla = [list(x) for x in s.split("\n")]
cvorovi = {}
for i in range(len(tabla)):
    for j in range(len(tabla[0])):
        if tabla[i][j] == "O":
            cvorovi[(i, j)] = []

i = j = 0

for vrste_dir in [-1, 1]:
    while 7 > i > -1:
        while 7 > j > -1:
            if tabla[i][j] == "O":
                k = vrste_dir
                while 7 > (j + k) > -1:
                    if tabla[i][j+k] == "O":
                        cvorovi[(i, j)].append((i, j+k))
                        break
                    k += vrste_dir
                j += 1
            else:
                j += 1
        i += 1
        j = 0
    i = j = 0
# for i, v in cvorovi.items():
#     print(i, v)
# print()
# print()
i = j = 0
for kolone_dir in [-1, 1]:
    while 7 > i > -1:
        while 7 > j > -1:
            if tabla[i][j] == "O":
                k = kolone_dir
                while 7 > (i + k) > -1:
                    if tabla[i+k][j] == "O":
                        cvorovi[(i, j)].append((i+k, j))
                        break
                    k += kolone_dir
                j += 1
            else:
                j += 1
        i += 1
        j = 0
    i = j = 0

for i, v in cvorovi.items():
    print(i, v)
